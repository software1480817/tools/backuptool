// JCL_DEBUG_EXPERT_GENERATEJDBG OFF
// JCL_DEBUG_EXPERT_INSERTJDBG OFF
// JCL_DEBUG_EXPERT_DELETEMAPFILE OFF
program OurPlantBackup;

{$APPTYPE CONSOLE}

{$R Version.res}

uses
  packages.tools.VersionInformation,
  System.SysUtils,
  System.IOUtils;

const
  cROOT = 'c:\z-Archiv';

{$I gitversion.inc}

// returning Version information from gitversion.inc
function GetVersionInformation: TExtractFileVersionData;
begin
  Result := VersionInformation.ExtractInformations;
end;

function GetVersionNumber: string;
begin
  Result := GetVersionInformation.FileVersionData.FileVersionNr;
end;

var
  vDestDir: string;
  fVersionInfo: string;
  fInfoRecord: TExtractFileVersionData;

begin

  fInfoRecord := VersionInformation.ExtractInformations;
  if VersionInformation.IsRelease then
    fVersionInfo := 'V' + fInfoRecord.FileVersionData.FileVersionNr
  else
    fVersionInfo := 'Entwicklerversion' + ' | ' + GIT_CURRENT_BRANCH + ' | ' + GIT_AUTHOR_NAME;

  try
    System.Writeln('Version: ' + fVersionInfo);
    vDestDir := TPath.Combine(cROOT, FormatDateTime('yyyy_mm_dd', now));
    if TDirectory.Exists(vDestDir) then
      vDestDir := TPath.Combine(cRoot, FormatDateTime('yyyy_mm_dd_hh_nn_ss', now));
    ForceDirectories(vDestDir);
    if DirectoryExists('c:\Arbeit') then
    begin
      System.Writeln('Kopiere Arbeit');
      TDirectory.Copy('c:\Arbeit\', TPath.Combine(vDestDir, 'Arbeit'));
    end;
    if DirectoryExists('c:\Data') then
    begin
      System.Writeln('Kopiere Data');
      TDirectory.Copy('c:\Data\', TPath.Combine(vDestDir, 'Data'));
    end;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
